use num::Complex;
use std::str::FromStr;

/// Parse the string `s` as a coordinate pair, like `"400x600"` or `"1.0,0.5"`
///
/// Specifically, `s` should be of the form <left><sep><right>
pub fn parse_pair<T: FromStr>(s: &str, separator: char) -> Option<(T, T)> {
    match s.find(separator) {
        None => None,
        Some(index) => match (T::from_str(&s[..index]), T::from_str(&s[index + 1..])) {
            (Ok(l), Ok(r)) => Some((l, r)),
            _ => None,
        },
    }
}

#[test]
fn test_parse_pair() {
    assert_eq!(parse_pair::<i32>("", ','), None);
    assert_eq!(parse_pair::<i32>("400,600", ','), Some((400, 600)));
    assert_eq!(parse_pair::<f64>("1.5,2.5", ','), Some((1.5, 2.5)));
}

/// Parse the string `s` as a complex number, with the real part first.
///
/// Specifically, an input of the form "1.2,0" would be parsed as the number
/// 1.2+0*i. Another example "3.2,1.2" --> 3.2 + 1.2*i.
pub fn parse_complex(s: &str) -> Option<Complex<f64>> {
    match parse_pair::<f64>(s, ',') {
        None => None,
        Some((re, im)) => Some(Complex { re, im }),
    }
}

#[test]
fn test_parse_complex() {
    assert_eq!(parse_complex("1.2,0"), Some(Complex { re: 1.2, im: 0.0 }));
    assert_eq!(parse_complex("3.2,1.2"), Some(Complex { re: 3.2, im: 1.2 }));
    assert_eq!(parse_complex("1.2,"), None);
}
