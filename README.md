# Mandelbrot in Rust

Following along O'Reilly's *Programming Rust*, this repository contains a Rust implementation for generating Mandelbrot pictures.
One such example is

![](mandel.png)

## Setup

Assumes you've used `rustup` to get Rust and so on.
This project uses `cargo`, so you can run commands like:

**Tests**

```bash
$ cargo test
```

**Build (Debug)**

```bash
$ cargo build
```

**Build (Release)**

```bash
$ cargo build --release
```